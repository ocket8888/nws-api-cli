/// <summary>Holds IDs for Zones that fall within a particular state.</summary>
/// <remarks>This may also be a province or territory e.g. Puerto Rico.</remarks>
public class State {
	/// <value>
	/// The two-character ISO state/province code for this State (or province).
	/// </value>
	public readonly string name;

	/// <value>
	/// Mapping of "public area" names to their respective zone IDs, within this
	/// State.
	/// </value>
	public Dictionary<string, string> publicAreas = new() {};

	public State(string name) {
		this.name = name;
	}

	/// <summary>
	/// Adds in the public areas and coastal/offshore areas present in the zone
	/// data to those stored as <see cref="publicAreas"/> on the State.
	/// </summary>
	public void add(ZoneListing.Zone zone) {
		if (zone.type is "public" or "coastal" or "offshore" && zone.atID.PathAndQuery.Contains("forecast")) {
			publicAreas.Add(zone.name, zone.id);
		}
	}
}
