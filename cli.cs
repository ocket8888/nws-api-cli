/// <summary>
/// A CLI instance is used to control access to the Command-Line interface
/// through abstractions for common tasks.
/// </summary>
public class CLI {
	/// <summary>
	/// Used internally when the user tries to enter something invalid according
	/// to the currently set completions.
	/// </summary>
	public class InvalidCompletionError : Exception {
		public InvalidCompletionError(string? message) : base(message) {}
	}
	public class Cancellation : Exception {}

	/// <summary>
	/// Checks if the standard input/output streams of the process are connected
	/// to an actual terminal.
	/// </summary>
	/// <remarks>
	/// Per
	/// https://stackoverflow.com/questions/3453220/how-to-detect-if-console-in-stdin-has-been-redirected/12527552#12527552
	/// the .NET runtime sets the Console window size to zero in this situation,
	/// both in MS's .NET Core and Mono. Whereas a check on
	/// <c>IsInputRedirected</c>/<c>IsOutputRedirected</c>/<c>IsErrorRedirected</c>
	/// may fail in some complex piping/job handling scenarios.
	/// </remarks>
	private static bool isTTY() {
		return Console.WindowHeight != 0 && Console.WindowWidth != 0;
	}

	/// <summary>
	/// Clears the screen - if the screen is a TTY. If it isn't, this is still
	/// safe to call, because it'll just do nothing.
	/// </summary>
	public static void clear() {
		if (isTTY()) {
			Console.Clear();
		}
	}

	private static CLI? cli;

	/// <summary>Gets a CLI instance.</summary>
	/// <remarks>
	/// Since STDOUT, STDERR, STDIN are all described by a  single file
	/// descriptor each, there really is only ever one CLI at any given time,
	/// which is why this class is a singleton.
	/// </remarks>
	public static CLI instance() {
		if (CLI.cli is null) {
			CLI.cli = new CLI();
		}
		return CLI.cli;
	}

	private readonly ConsoleColor initialBackground;
	private readonly ConsoleColor initialForeground;

	private CLI() {
		this.initialBackground = Console.BackgroundColor;
		this.initialForeground = Console.ForegroundColor;
	}

	/// <summary>Restores terminal colors to their native values.</summary>
	public void restoreColor() {
		Console.BackgroundColor = this.initialBackground;
		Console.ForegroundColor = this.initialForeground;
	}

	/// <summary>The current foreground text color of the terminal.</summary>
	public ConsoleColor ForegroundColor {
		get => Console.ForegroundColor;
		set => Console.ForegroundColor = value;
	}

	/// <summary>The current background text color of the terminal.</summary>
	public ConsoleColor BackgroundColor {
		get => Console.ForegroundColor;
		set => Console.ForegroundColor = value;
	}

	private IEnumerable<string>? completions;

	/// <summary>Sets up tab completions to use the given collection.</summary>
	public void setCompletions(IEnumerable<string>? completions) {
		this.completions = completions;
	}

	/// <summary>
	/// Gets the longest common base of some string set - assuming they all
	/// begin with <paramref name="minimum"/>.
	/// <example>
	/// For example:
	/// <code>
	/// string common = greatestCommonBase("mor", new string[]{"more", "moreover"});
	/// </code>
	/// results in <c>common</c> having the value <c>"more"</c>.
	/// </example>
	/// </summary>
	/// <param name="minimum">
	/// The user's input; the given completions collection MUST all start with
	/// this.
	/// </param>
	/// <param name="collection">
	/// A collection of strings that all start with <paramref name="minimum"/>
	/// but may also all begin with a longer string.
	/// </param>
	/// <exception cref="InvalidCompletionError">
	/// when the set of matching strings provided by
	/// <paramref name="collection"/> is empty.
	/// </exception>
	private static string greatestCommonBase(string minimum, IEnumerable<string> collection) {
		if (collection.Count() < 1) {
			throw new InvalidCompletionError($"'{minimum}' doesn't match any valid options");
		}

		var greatest = collection.ElementAt(0).Skip(minimum.Length);
		foreach (string str in collection.Skip(1)) {
			var current = str.Skip(minimum.Length);
			string buffer = "";
			for (int i = 0; i < current.Count() && i < greatest.Count(); ++i) {
				char currentChar = current.ElementAt(i);
				if (currentChar != greatest.ElementAt(i)) {
					break;
				}
				buffer += currentChar;
			}
			greatest = buffer;
		}

		return minimum + new String(greatest.ToArray());
	}

	/// <summary>
	/// Handles printing the prompt - with optional postscript - for
	/// <see cref="getInteractiveInput"/>.
	/// </summary>
	/// <param name="prompt">The base input prompt.</param>
	/// <param name="currentEntry">What the user has currently entered.</param>
	/// <param name="postscript">
	/// A list of lines to print after the prompt line. If there are too many to
	/// print in the visible space of the terminal, later entries will be cut
	/// off. Creating columns is too much work for this project.
	/// </param>
	/// <remarks>
	/// The ending cursor position after calling this method is at the end of
	/// the user's input following the prompt - which will almost certainly be
	/// on the first line of the terminal.
	/// </remarks>
	private void printPrompt(string prompt, string currentEntry, IEnumerable<string>? postscript) {
		if (postscript is not null) {
			var pos = Console.GetCursorPosition();
			++Console.CursorTop;
			this.ForegroundColor = ConsoleColor.Gray; // TODO: This should actually just darken whatever the current foreground is
			for (int i = 0; i < postscript.Count(); ++i) {
				if (pos.Top + i > Console.WindowHeight) {
					break;
				}
				string post = postscript.ElementAt(i);
				Console.WriteLine(post);
			}
			this.restoreColor();
			Console.SetCursorPosition(pos.Left, pos.Top);
		}
		Console.Write(prompt);
		Console.Write(" ");
		Console.Write(currentEntry);
	}

	/// <summary>
	/// Gets input from the user interactively. That is, using TTY functionality
	/// (which is why this is private - it must only be used on TTYs).
	/// </summary>
	/// <exception cref="Cancellation">
	/// if the user cancels selection with the "Escape" or "Back" (NOT
	/// "Backspace") key.
	/// </exception>
	private string getInteractiveInput(string prompt) {
		Console.Clear();

		string buffer = "";
		IEnumerable<string> completions = this.completions is null ? new string[]{} : this.completions;
		var recomplete = () => {
			completions = completions.Where(c => c.StartsWith(buffer));
		};
		IEnumerable<string>? post = null;
		ConsoleKeyInfo? last = null;
		while (true) {
			printPrompt(prompt, buffer, post);
			post = null;

			try {
				var key = Console.ReadKey();
				switch(key.Key) {
					case ConsoleKey.Enter:
						if (this.completions == null) {
							return buffer;
						}
						if (completions.Count() == 1) {
							return completions.ElementAt(0);
						}
						if (completions.Contains(buffer)) {
							return buffer;
						}
						var err = new InvalidCompletionError($"invalid selection: '{buffer}'");
						buffer = "";
						completions = this.completions is null ? new string[]{} : this.completions;
						throw err;
					case ConsoleKey.Backspace:
						buffer = buffer.Substring(0, buffer.Length > 0 ? buffer.Length - 1 : 0);
						completions = this.completions is null ? new string[]{} : this.completions;
						break;
					case ConsoleKey.Tab:
						if (last is not null && ((ConsoleKeyInfo)last).Key is ConsoleKey.Tab) {
							post = completions;
						} else {
							buffer = greatestCommonBase(buffer, completions);
						}
						break;
					case ConsoleKey.Escape:
					case ConsoleKey.BrowserBack:
						throw new Cancellation();
					default:
						// TODO: Handle these?
						if (key.Modifiers is ConsoleModifiers.Shift or 0 && !Char.IsControl(key.KeyChar)) {
							buffer += key.KeyChar;
						}
						break;
				}
				last = key;
				recomplete();
				Console.Clear();
			} catch (InvalidCompletionError e) {
				Console.Clear();
				this.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine(e.Message);
				this.restoreColor();
			}
		}
	}

	/// <summary>
	/// Gets input from the user with the given prompt.
	/// </summary>
	/// <param name="prompt">
	/// A prompt to use so the user knows what to enter. If not given, a generic
	/// message will be presented instead.
	/// </param>
	/// <exception cref="Cancellation">
	/// if the user cancels selection with the "Escape" or "Back" (NOT
	/// "Backspace") key.
	/// </exception>
	public string getInput(string? prompt) {
		prompt = prompt is null ? "Enter selection:" : prompt.TrimEnd();
		if (!prompt.EndsWith(":")) {
			prompt += ":";
		}
		if (!isTTY()) {
			string? input = null;
			Func<bool> check = () => {
				if (input is null or "" || this.completions is null || this.completions.Count() < 1) {
					return true;
				}
				return this.completions.Contains(input);
			};
			while (check()) {
				Console.Write(prompt);
				input = Console.ReadLine();
			}
			return input!;
		}
		return this.getInteractiveInput(prompt);
	}

	/// <summary>
	/// Gets the user to choose something from a given list.
	/// <summary>
	/// <exception cref="Cancellation">
	/// if the user cancels selection with the "Escape" or "Back" (NOT
	/// "Backspace") key.
	/// </exception>
	/// <remarks>
	/// This will set up completions for the given collection temporarily, then
	/// restore them when the selection is complete. Because of this (and also
	/// by nature of a stateful singleton just inherently) this isn't safe for
	/// concurrent use.
	/// </remarks>
	public string getInputFromCollection(IEnumerable<string> collection, string? prompt) {
		var original = completions;
		setCompletions(collection);
		var input = getInput(prompt);
		setCompletions(original);
		return input;
	}

	/// <summary>Pretty-prints the forecast to a TTY on stdout.</summary>
	private void printForeCastToTTY(Forecast forecast) {
		clear();
		foreach (Period p in forecast.periods) {
			int maxLen = p.detailedForecast.Length;
			if (Console.CursorLeft + maxLen > Console.WindowWidth) {
				Console.CursorLeft = 0;
				Console.CursorTop += 3;
			}
			int initalPos = Console.CursorLeft;
			int namePos = (maxLen / 2) - (p.name.Length / 2);
			Console.CursorLeft += namePos;
			Console.Write(p.name);
			Console.CursorLeft = initalPos;
			Console.CursorTop += 1;
			Console.Write(p.detailedForecast);
			Console.Write(" | ");
			Console.CursorTop -= 1;
			Console.CursorLeft -= 3;
			Console.Write(" | ");
			// Console.CursorLeft += 1;
		}
	}

	/// <summary>Prints out a forecast for the user to see.</summary>
	public void printForecast(Forecast forecast) {
		if (!isTTY()) {
			foreach (Period p in forecast.periods) {
				Console.WriteLine(p.name);
				Console.WriteLine(p.detailedForecast);
				Console.WriteLine();
			}
			return;
		}
		printForeCastToTTY(forecast);
	}
}
