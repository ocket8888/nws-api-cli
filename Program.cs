﻿
internal class Program {
	/// <value>The version of this program.</value>
	private static readonly string VERSION = "0.1.0";

	private static CLI cli = CLI.instance();

	private static async Task<Dictionary<string, State>> setupStates(WeatherClient client) {
		ZoneListing zones = await client.getZones();
		Dictionary<string, State> states = new Dictionary<string, State>();

		foreach (ZoneListing.Feature feature in zones.features) {
			if (feature.properties.state is null or "") {
				continue;
			}
			State? state;
			if (!states.TryGetValue(feature.properties.state, out state)) {
				state = new State(feature.properties.state);
				states.Add(state.name, state);
			}
			state.add(feature.properties);
		}
		return states;
	}

	private static async Task mainLoop(WeatherClient client, Dictionary<string, State> states) {
		while (true) {
			Console.Clear();

			string choice;
			try {
				choice = cli.getInputFromCollection(states.Keys, "Pick a state (ESC to cancel):");
			} catch (CLI.Cancellation) {
				return;
			}

			State state = states[choice];

			while (true) {
				IEnumerable<string> places = state.publicAreas.Keys;
				try {
					choice = cli.getInputFromCollection(places, "Pick county/area (ESC to go back):");
				} catch (CLI.Cancellation) {
					break;
				}

				string zoneID = state.publicAreas[choice];
				cli.printForecast(await client.getForecast(zoneID));
				Console.Read();
			}
		}
	}

	private static async Task<int> Main(string[] args) {
		WeatherClient client = new ("weather-cli .net/" + VERSION);
		try {
			string response = await client.getStatus();
			if (response != "OK") {
				Console.Error.WriteLine($"status '{response}' is not 'OK'");
				return 2;
			}

			var states = await setupStates(client);
			await mainLoop(client, states);
			return 0;
		} catch (WeatherClient.WeatherClientException e) {
			Console.Error.WriteLine(e);
			return 1;
		}
	}
}
