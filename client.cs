using System.Diagnostics.CodeAnalysis;
using System.Net.Http.Json;
using System.Text.Json.Serialization;

/// <summary>
/// The type of response returned from the weather.gov API's root.
/// </summary>
class StatusIndication {
	public string status {get; set;} = "";
}

/// <summary>The type of responses to requests for lists of Zones.</summary>
public class ZoneListing {
	/// <summary>
	/// The type of the <c>@context</c> property of a Zone listing.
	/// </summary>
	public class ZoneListingContext {
		[JsonPropertyName("@version")]
		public string atVersion {get; set;} = "";
	}
	/// <summary>A single Zone in a Zone listing.</summary>
	public class Zone {
		[JsonPropertyName("@id")]
		public Uri atID {get; set;} = null!;
		[JsonPropertyName("@type")]
		public string atType {get; set;} = "";
		public string id {get; set;} = "";
		public string type {get; set;} = "";
		public string name {get; set;} = "";
		public DateTime effectiveDate {get; set;}
		public DateTime expirationDate {get; set;}
		public string? state {get; set;}
		public string[] cwa {get; set;} = new string[]{};
		public string[] forecastOffices {get; set;} = new string[]{};
		public string[] observationStations {get; set;} = new string[]{};
		public string[] timezone {get; set;} = new string[]{};
		public string? radarStation {get; set;}
	}

	/// <summary>A single "Feature" in a Zone listing.</summary>
	public class Feature {
		public Uri id {get; set;} = null!;
		/// <value>A field of unknown type and purpose</value>
		/// <remarks>I've never seen this not be null.</remarks>
		public object? geometry {get; set;}
		public Zone properties {get; set;} = new Zone();
	}

	[JsonPropertyName("@context")]
	public ZoneListingContext atContext {get; set;} = new ZoneListingContext();
	public string type {get; set;} = "";
	public Feature[] features {get; set;} = Array.Empty<Feature>();
}

/// <summary>A single day/part of a day in a forecast.</summary>
public class Period {
	public uint number {get; set;} = 0;
	public string name {get; set;} = "";
	public string detailedForecast {get; set;} = "";
}

/// <summary>Represents forecasted weather for some zone.</summary>
public class Forecast {
	public Uri zone {get; set;} = null!;
	public DateTime updated {get; set;}
	public Period[] periods {get; set;} = Array.Empty<Period>();
}

/// <summary>
/// The type of a raw response from the /zones/forecast/{{ID}}/forecast API
/// endpoint.
/// </summary>
class ForecastResponse {
	public Forecast properties {get; set;} = null!;
}
/// <summary>
/// A thin HttpClient wrapper that provides access to the weather.gov API.
/// </summary>
public class WeatherClient {

	/// <summary>
	/// An exception thrown by the WeatherClient when the API doesn't work as
	/// expected. Base exceptions may contain more information.
	/// </summary>
	public class WeatherClientException : Exception {
		/// <value>
		/// The URL that was being requested when the failure occurred.
		/// <value>
		public readonly string requestURL;
		/// <summary><see cref="System.Exception.Exception(string?)"/></summary>
		/// <param name="requestURL">
		/// The (relative) URL that was being requested.
		/// </param>
		public WeatherClientException(string requestURL, string message) : base(message) {
			this.requestURL = requestURL;
		}
		/// <summary><see cref="System.Exception.Exception(string?, Exception?)"/></summary>
		/// <param name="requestURL">
		/// The (relative) URL that was being requested.
		/// </param>
		public WeatherClientException(string requestURL, string message, Exception e) : base(message, e) {
			this.requestURL = requestURL;
		}
	}

	/// <value>
	/// A URI defining the location of the weather site to which
	/// requests are sent.
	/// </value>
	public static readonly Uri BASE_URL = new Uri("https://api.weather.gov");
	/// <value>The string being sent in the User-Agent header.</value>
	public readonly string USER_AGENT;

	private readonly HttpClient httpClient;

	/// <summary>
	/// Builds a new weather client. This client will always deserialize as
	/// JSON, and only makes requests to the WeatherClient.BASE_URL.
	/// </summary>
	/// <param name="uaStringBase">The base user-agent string to use when
	/// identifying the program to weather.gov. Some information about the .NET
	/// runtime is added to it.</param>
	public WeatherClient(string uaStringBase) {
		this.USER_AGENT = uaStringBase + " " + System.Runtime.InteropServices.RuntimeInformation.FrameworkDescription;
		this.httpClient = new(){
			BaseAddress = WeatherClient.BASE_URL,
			DefaultRequestHeaders = {
				{"Accept", "application/json, application/geo+json;q=0.9"},
				{"Accept-Charset", "UTF-8"},
				{"User-Agent", this.USER_AGENT}
			}
		};
	}

	/// <summary>
	/// Sends an HTTP GET request to the weather API, returning the deserialized
	/// response.
	/// </summary>
	/// <param name="path">The request path, e.g. <c>/</c>.</param>
	/// <exception cref="WeatherClientException">
	/// when the API returns something unexpected and/or cannot be reached.
	/// </exception>
	private async Task<T> get<T>(string path) {
		try {
			T? response = await this.httpClient.GetFromJsonAsync<T>(path);
			if (response is null) {
				throw new WeatherClientException(path, "weather API returned an unexpectedly null response");
			}
			return response;
		} catch (Exception e) {
			if (e is System.Text.Json.JsonException || e is HttpRequestException) {
				throw new WeatherClientException(path, e.Message, e);
			}
			throw;
		}
	}

	/// <summary>
	/// Gets the status of the weather.gov API and your connection to it.
	/// </summary>
	/// <returns>A string that the API used to describe its status</returns>
	/// <exception cref="WeatherClientException">
	/// when the API returns something unexpected and/or cannot be reached.
	/// </exception>
	public async Task<string> getStatus() {
		StatusIndication statusIndication = await get<StatusIndication>("/");
		return statusIndication.status;
	}

	/// <summary>
	/// Gets a list of zones monitored for weather data by the National Weather
	/// Service.
	/// </summary>
	public async Task<ZoneListing> getZones() {
		ZoneListing zl = await get<ZoneListing>("/zones");
		return zl;
	}

	/// <summary>
	/// Produces a forecast for the specified Zone.
	/// </summary>
	public async Task<Forecast> getForecast(string zone) {
		ForecastResponse response = await get<ForecastResponse>($"/zones/forecast/{zone}/forecast");
		return response.properties;
	}
}
